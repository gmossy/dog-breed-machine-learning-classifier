# Dog Breed Machine Learning Classifier

This app will recognize images that are a dog or human face, then use machine learning and a deep learning architecture to identify the breed of the dog.  It will also provide likeness to a dog breed if a human face is provided. 